#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/openjdk:11-jre-slim-bullseye"

echo "${IMAGE}"
